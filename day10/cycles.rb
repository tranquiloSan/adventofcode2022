#!/usr/bin/env ruby

$CRT_Width = 40
$Sprite_Length = 3

def draw_pixel(crt, sprite_position, cycle)
    # create new line in crt if needed
    line = cycle / $CRT_Width
    pos = (cycle % $CRT_Width) - 1
    pos = 0 if pos < 0
    crt[line] = [ ] if pos == 0
    puts "During cycle\t#{cycle}: CRT draws pixel in position #{pos}"

    sprite_position = 0 if sprite_position < 0
    if pos.between?(sprite_position, sprite_position + $Sprite_Length - 1)
        crt[line][pos] = '█'
    else
        crt[line][pos] = '.'
    end
    puts "Current CRT row: #{crt[line].join}"
end

cycle = 0
sprite_position = 1
crt = { 0 => [] }

puts "Sprite position: #{sprite_position}"

File.readlines('input.txt').each do |line|
    addx = 0

    cycle += 1
    puts "Start cycle\t#{cycle}: begin executing #{line}"
    draw_pixel(crt, sprite_position - 1, cycle)

    if /addx ([+-]?\d+)/.match(line)
        cycle += 1
        draw_pixel(crt, sprite_position - 1, cycle)

        sprite_position += $1.to_i
    end
    puts "End of cycle #{cycle}: finish executing #{line}"
    puts "Sprite position: #{sprite_position}"
end

crt.each do |line, pixels|
    puts pixels.join
end


###...##..###..###..#..#..##..###....##
#..#.#..#.#..#.#..#.#.#..#..#.#..#....#
##.#.#....#..#.###..##...#..#.#..#....#
###..#....###..#..#.#.#..####.###.....#
##...#..#.#....#..#.#.#..#..#.#....#..#
#.....##..#....###..#..#.#..#.#.....##.