#!/bin/python3

d = {
    'A' : {
        'X': 0 + 3,
        'Y': 3 + 1,
        'Z': 6 + 2,
    },
    'B' : {
        'X': 0 + 1,
        'Y': 3 + 2,
        'Z': 6 + 3,
    },
    'C' : {
        'X': 0 + 2,
        'Y': 3 + 3,
        'Z': 6 + 1,
    }
}

score = 0
file = open('input.txt', 'r')
for line in file.readlines():
    line = line.strip()
    print(line[0])
    score += d[line[0]][line[2]]

print(score)