package main

import (
	"os"
	"fmt"
	"bufio"
	"strconv"
	"sort"
)

func main() {
	file, _ := os.Open("./input.txt")
	defer file.Close()

	var calories []int

	scanner := bufio.NewScanner(file)
	elf := 0
	calories = append(calories, 0)

	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(line)
		if line == "" {
			elf++
			calories = append(calories, 0)
			continue
		}

		num, _ := strconv.Atoi(line)
		calories[elf] += num
	}

	sort.Sort(sort.Reverse(sort.IntSlice(calories)))
	fmt.Println(calories)
	fmt.Printf("Top elf: %v\n", calories[0])
	fmt.Printf("Top three elves: %v + %v + %v = %v\n", calories[0], calories[1], calories[2], calories[0] + calories[1] + calories[2])
}