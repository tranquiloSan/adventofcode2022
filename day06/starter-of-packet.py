#!/bin/python3

START_OF_PACKET = 4
START_OF_MESSAGE = 14

file = open('input.txt', 'r')
line = file.readline()

MARKER_LEN = START_OF_MESSAGE

for i in range(0, len(line)):
    chars = []

    for j in range(0, MARKER_LEN):
        chars.append(line[i+j])

    if len(set(chars)) == MARKER_LEN:
        print(line[i:i+MARKER_LEN], i+MARKER_LEN)
        break