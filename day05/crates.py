#!/bin/python3

# [B]                     [N]     [H]
# [V]         [P] [T]     [V]     [P]
# [W]     [C] [T] [S]     [H]     [N]
# [T]     [J] [Z] [M] [N] [F]     [L]
# [Q]     [W] [N] [J] [T] [Q] [R] [B]
# [N] [B] [Q] [R] [V] [F] [D] [F] [M]
# [H] [W] [S] [J] [P] [W] [L] [P] [S]
# [D] [D] [T] [F] [G] [B] [B] [H] [Z]
#  1   2   3   4   5   6   7   8   9 

# creates = [
#     ['B', 'V', 'W', 'T', 'Q', 'N', 'H', 'D'],
#     ['B', 'W', 'D']
# ]

import re

file = open('input.txt', 'r')
lines = file.readlines()

input_creates = []
for num_of_lines, line in enumerate(lines):
    input_creates.append([])
    i = 0
    while i < len(line):
        input_creates[num_of_lines].append(line[i:i+3])
        i+=4

    if re.match(r'^ *(\d+ *)+$', line):
        break

stacks = []
idx = 0
while len(stacks) <= num_of_lines:
    stacks.append([])
    for creates in input_creates:
        try:
            int(creates[idx])
            break
        except (ValueError, IndexError):
            pass

        if creates[idx][1] != ' ':
            stacks[idx].append(creates[idx][1])

    idx += 1


for stack in stacks:
    print(stack)

for line in lines[10:]:
    match = re.match('move (\d+) from (\d+) to (\d+)', line)
    create_count = int(match[1])
    stack_from = int(match[2]) - 1
    stack_to = int(match[3]) - 1

    print(line)
    creates_to_move = []
    for i in range(create_count):
        creates_to_move.insert(0, stacks[stack_from].pop(0))

    for create in creates_to_move:
        stacks[stack_to].insert(0, create)

print("result:")
for stack in stacks:
    print(stack)

for stack in stacks:
    print(stack[0], end='')
print()