#!/usr/bin/env python3

def should_move(pos1, pos2):
    if pos1 == pos2:
        return False

    diff_x = abs(pos1[0] - pos2[0])
    diff_y = abs(pos1[1] - pos2[1])
    max_dist = 1

    if diff_x == 0 and diff_y <= max_dist:
        return False
    elif diff_y == 0 and diff_x <= max_dist:
        return False
    elif diff_x <= max_dist and diff_y <= max_dist:
        return False

    return True

file = open("input.txt", "r")

start = [0, 0]
head = start.copy()
tail = start.copy()
visited = set()

for line in file.readlines():
    where, times = line.split()
    times = int(times)

    if where == "R":
        for i in range(times):
            last = head.copy()
            head[0] += 1
            if should_move(head, tail):
                tail = last
            visited.add(tuple(tail))
    elif where == "L":
        for i in range(times):
            last = head.copy()
            head[0] -= 1
            if should_move(head, tail):
                tail = last
            visited.add(tuple(tail))
    elif where == "U":
        for i in range(times):
            last = head.copy()
            head[1] += 1
            if should_move(head, tail):
                tail = last
            visited.add(tuple(tail))
    elif where == "D":
        for i in range(times):
            last = head.copy()
            head[1] -= 1
            if should_move(head, tail):
                tail = last
            visited.add(tuple(tail))

print(len(visited))