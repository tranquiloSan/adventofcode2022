#!/bin/python3

file = open("input.txt", "r")

tree_grid = []

# load tree grid
for i, line in enumerate(file.readlines()):
    tree_grid.append([])
    for char in line:
        if char != '\n':
            tree_grid[i].append(int(char))

visible = 0

scores = []
for i in range(len(tree_grid)):
    for j in range(len(tree_grid[i])):
        view = {
            'top': 0,
            'bottom': 0,
            'left': 0,
            'right': 0,
        }

        tree = tree_grid[i][j]

        # top
        if i == 0: 
            view['top'] = 0

        # bottom
        if i == len(tree_grid) - 1:
            view['bottom'] = 0
            
        # left
        if j == 0:
            view['left'] = 0

        # right
        if j == len(tree_grid[i]) - 1:
            view['right'] = 0

        # inner top
        for x in range(i-1, 0-1, -1):
            view['top'] += 1
            if tree_grid[x][j] >= tree:
                break

        # inner bottom
        for x in range(i+1, len(tree_grid)):
            view['bottom'] += 1
            if tree_grid[x][j] >= tree:
                break

        # inner left
        for y in range(j-1, 0-1, -1):
            view['left'] += 1
            if tree_grid[i][y] >= tree:
                break

        # inner right
        for y in range(j+1, len(tree_grid[i])):
            view['right'] += 1
            if tree_grid[i][y] >= tree:
                break

        scenic_score = view['bottom'] * view['left'] * view['right'] * view['top']
        scores.append(scenic_score)

print(max(scores))
