#!/bin/python3

abc = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
       'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',]

def find_common_item():
    with open("input.txt", "r") as file:
        priority = 0
        for line in file.readlines():
            line = line.strip()
            if line == "":
                continue
            half = len(line) // 2
            rucksack1 = line[:half]
            rucksack2 = line[half:]
            print(line)
            for item in rucksack1:
                if item in rucksack2:
                    print("item: " + item + "(" + str(abc.index(item) + 1) + ")")
                    priority += abc.index(item) + 1
                    break
        print(priority)

def find_badge():
    with open("input.txt", "r") as file:
        priority = 0
        lines = file.readlines()
        while len(lines) > 0:
            line1 = lines[0].strip()
            line2 = lines[1].strip()
            line3 = lines[2].strip()
            print(line1, line2, line3)
            for c in line1:
                if c in line2 and c in line3:
                    print("badge: " + c + "(" + str(abc.index(c) + 1) + ")")
                    priority += abc.index(c) + 1
                    break
            lines = lines[3:]
        print(priority)

find_badge()