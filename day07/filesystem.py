#!/bin/python

class Node:
    def __init__(self, name, size, parent):
        self.name = name
        self.size = int(size)
        self.parent = parent
        self.children = []

    def add_child(self, child):
        self.children.append(child)

    def get_path(self):
        if self.parent is None:
            return '/'
        path = []
        node = self
        while node:
            path.append(node.name)
            node = node.parent
        return '/'.join(reversed(path))[1:]

    def search(self, name):
        for child in self.children:
            if child.name == name:
                return child

    def print_tree(self, level=0):
        filetype = 'dir' if self.size == 0 else f'file, size={self.size}'
        padding = "  " * level
        print(f'{padding}- {self.name} ({filetype})')
        for child in self.children:
            child.print_tree(level + 1)

    def file_size(self):
        if self.size != 0:
            return self.size
        size = 0
        for child in self.children:
            size += child.file_size()
        return size

    def get_directories(self):
        if self.size != 0:
            return []
        dirs = []
        for child in self.children:
            if self.size == 0:
                if self not in dirs:
                    dirs.append(self)
            dirs.extend(child.get_directories())
        return dirs

root = Node('/', 0, None)
file = open('input.txt', 'r')

prev_node = None
current_node = root
for line in file.readlines():
    line = line.strip()
    if line.startswith("$ "):
        cmd = line[2:].split(' ')
        if cmd[0] == 'ls':
            continue
        if cmd[0] == 'cd':
            if cmd[1] == '..':
                if not current_node.parent:
                    continue
                current_node = current_node.parent
            else:
                for child in current_node.children:
                    if child.name == cmd[1]:
                        current_node = child
                        break
                # current_node = current_node.search(cmd[1])
        continue

    size, name = line.split(' ')
    if size == 'dir': size = 0
    current_node.add_child(Node(name, size, current_node))


FILESYSTEM_SIZE = 70000000
UPDATE_SIZE = 30000000

free = FILESYSTEM_SIZE - root.file_size()
needed = UPDATE_SIZE - free

sizesum = 0
suitable = []
for d in root.get_directories():
    if d.file_size() >= needed:
        suitable.append(d)

suitable = sorted(suitable, key=lambda x: x.file_size())
print(suitable[0].name, suitable[0].file_size())
