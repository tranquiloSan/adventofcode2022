#!/bin/python3

def get_range(sections):
    min, max = sections.split('-')
    return [n for n in range(int(min), int(max) + 1)]

def full_overlap(list1, list2):
    return all(item in list1 for item in list2) or \
           all(item in list2 for item in list1)

def partial_overlap(list1, list2):
    return any(item in list1 for item in list2)

fully_contains = 0
partially_constains = 0

file = open("input.txt", "r")
for line in file.readlines():
    sections1, sections2 = line.strip().split(',')
    range1 = get_range(sections1)
    range2 = get_range(sections2)

    if partial_overlap(range1, range2):
        partially_constains += 1
        if full_overlap(range1, range2):
            fully_contains += 1

print(f"fully: {fully_contains}")
print(f"partially: {partially_constains}")